package ma.microservices.multiplication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;
import ma.microservices.multiplication.domain.Multiplication;
import ma.microservices.multiplication.service.MultiplicationService;

/**
 * This class implements a REST API for our Multiplication application.
 */
@Slf4j
@RestController
@RequestMapping("/multiplications")
@CrossOrigin(origins = "http://localhost:9090")
final class MultiplicationController {

	private final MultiplicationService multiplicationService;

	private final int serverPort;

	@Autowired
	public MultiplicationController(final MultiplicationService multiplicationService,
			@Value("${server.port}") int serverPort) {
		this.multiplicationService = multiplicationService;
		this.serverPort = serverPort;
	}

	@GetMapping("/random")
	public Multiplication getRandomMultiplication() {
		log.info("Generating a random multiplication from server @ {}", serverPort);
		return multiplicationService.createRandomMultiplication();
	}

}